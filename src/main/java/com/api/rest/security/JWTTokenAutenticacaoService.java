package com.api.rest.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.api.rest.ApplicationContextLoad;
import com.api.rest.model.Usuario;
import com.api.rest.repository.UsuarioRepository;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JWTTokenAutenticacaoService {

	// tempo de validade do token.. 2 dias
	private static final long EXPIRATION_TIME = 172800000;

	// uma senha unica para compor a autenticação e ajudar na segurança
	private static final String SECRET = "senhagod";

	// prefixo padrao de token
	private static final String TOKEN_PREFIX = "Bearer";

	private static final String HEADER_STRING = "Authorization";

	// Gerando token de autenticado e adicionado ao cabeçalho e resposta Http
	public void addAuthentication(HttpServletResponse response, String username) throws IOException {

		// Montagem do toke
		String JWT = Jwts.builder() // chama o gerador de token
				.setSubject(username) // adiciona o usuario
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))// tempo de expiraçao
				.signWith(SignatureAlgorithm.HS512, SECRET).compact(); // compactacao de algoritimos de geracao de senha

		// Junta o token com o prefixo
		String token = TOKEN_PREFIX + " " + JWT; // BEARER 21asdasd89

		// adiciona no cabeçalho de HTTP
		response.addHeader(HEADER_STRING, token);// authorization: asd78a9sdf

		// Liberando resposta para as portas diferentes que usam a API ou caso clientes
		// webs
		liberarCORS(response);

		// escreve token como resposta no corpo http
		response.getWriter().write("{\"Authorization\": \"" + token + "\"}");

	}

	// retorna o usuario validado com token ou caso nao seja valido retorna null
	public Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) {

		// pega o token enviado no cabeçalho http
		String token = request.getHeader(HEADER_STRING);
		try {

			if (token != null) {

				String tokenLimpo = token.replace(TOKEN_PREFIX, "").trim();

				// faz a validacao do token do usuario na requisiçao
				String user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(tokenLimpo).getBody().getSubject();

				if (user != null) {

					Usuario usuario = ApplicationContextLoad.getApplicationContext().getBean(UsuarioRepository.class)
							.findUserByLogin(user);

					if (usuario != null) {
						if (tokenLimpo.equalsIgnoreCase(usuario.getToken())) {
							return new UsernamePasswordAuthenticationToken(usuario.getLogin(), usuario.getSenha(),
									usuario.getAuthorities());
						}
					}

				}

			}
		} catch (io.jsonwebtoken.ExpiredJwtException e) {
			try {
				response.getOutputStream().println("Seu Token está expirado, faça o login ou infome um novo token!");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		liberarCORS(response);
		return null;
	}

	// CORS policy
	private void liberarCORS(HttpServletResponse response) {
		if (response.getHeader("Access-Control-Allow-Origin") == null) {
			response.addHeader("Access-Control-Allow-Origin", "*");
		}

		if (response.getHeader("Access-Control-Allow-Headers") == null) {
			response.addHeader("Access-Control-Allow-Headers", "*");
		}

		if (response.getHeader("Access-Control-Request-Headers") == null) {
			response.addHeader("Access-Control-Request-Headers", "*");
		}

		if (response.getHeader("Access-Control-Allow-Methods") == null) {
			response.addHeader("Access-Control-Allow-Methods", "*");
		}
	}

}

package com.api.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@CrossOrigin(origins = "*")
//@EntityScan(basePackages = {"curso.api.rest.model"})
//@ComponentScan("curso.*")
//@EnableJpaRepositories(basePackages = {"curso.api.rest.repository"})
//@EnableTransactionManagement
//@EnableWebMvc
@EnableCaching


@RestController
public class CursospringrestapiApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(CursospringrestapiApplication.class, args);
	}

	// MAPEAMENTO GLOBAL QUE REFLETEM EM TOODO OSISTEMA
	// @Override
	// public void addCorsMappings(CorsRegistry registry) {
	// registry.addMapping("/usuario/**").allowedMethods("POST", "PUT", "DELETE")
	// .allowedOrigins("www.cliente.com.br");
	// LIBERANDO APENAS REQUISOÇOES PARA O USUARIO DO SERVIDOR CLIENTE
	// }

	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/usuario/**").allowedMethods("POST", "PUT", "DELETE", "GET")
				.allowedOrigins("localhost:8080");
		//System.out.println(new BCryptPasswordEncoder().encode("123"));
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

}

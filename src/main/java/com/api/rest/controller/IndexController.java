package com.api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.rest.model.UsarioDTO;
import com.api.rest.model.Usuario;
import com.api.rest.repository.UsuarioRepository;

//@CrossOrigin(origins = "*") //LIBERA A API PARA TODOS OS DOMINIOS
@RestController
@RequestMapping(value = "/usuario")
public class IndexController {

	@Autowired // SE FOSSE CDI SERIA @INJECT
	private UsuarioRepository usuarioRepository;


	// SERVICE RESTFULL
	// @CrossOrigin(origins = "*") NOS METODOS, LIBERA APENAS OS ENDPOINTS
	@GetMapping(value = "v1/{id}", produces = "application/json")
	public ResponseEntity<UsarioDTO> findByIdV1(@PathVariable(value = "id") Long id) {

		Usuario usuario = usuarioRepository.findUserById(id);

		return new ResponseEntity<UsarioDTO>(new UsarioDTO(usuario), HttpStatus.OK);
	}

	@GetMapping(value = "v2/{id}", produces = "application/json")
	public ResponseEntity<Usuario> findByIdV2(@PathVariable(value = "id") Long id) {

		Usuario usuario = usuarioRepository.findUserById(id);

		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}/codigo/{venda}", produces = "application/json")
	public ResponseEntity<Usuario> relatorio(@PathVariable(value = "id") Long id,
			@PathVariable(value = "venda") Long venda) {

		Usuario usuario = usuarioRepository.findUserById(id);

		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}

	// VAMOS SUPOR QUE O CARREGAMENTO DE USUARIO SEJA UM PROCESSO MUITO LENTO
	// E QUEREMOS CONTROLAR ELE COM CACHE PARA AGILIZAR O PROCESSO
	@GetMapping(value = "/", produces = "application/json")
	@CacheEvict(value = "cacheusuario", allEntries =	 true)
	@CachePut("cacheusuario")
	public ResponseEntity<List<Usuario>> findAll() throws InterruptedException {
		List<Usuario> list = usuarioRepository.findAll();

		return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);

	}

	@PostMapping(value = "/", produces = "application/json")
	public ResponseEntity<Usuario> cadastrar(@RequestBody Usuario usuario) {
		usuario.getTelefoneslist().forEach(t -> t.setUsuario(usuario));

		String senhaCript = new BCryptPasswordEncoder().encode(usuario.getSenha());
		usuario.setSenha(senhaCript);
		Usuario usuarioSalvo = usuarioRepository.save(usuario);

		return new ResponseEntity<Usuario>(usuarioSalvo, HttpStatus.OK);
	}

	@PutMapping(value = "/", produces = "application/json")
	public ResponseEntity<Usuario> atualizar(@RequestBody Usuario usuario) {

		Usuario userTemp = usuarioRepository.findUserById(usuario.getId());
		usuario.getTelefoneslist().forEach(t -> t.setUsuario(usuario));

		if ((!userTemp.getSenha().equals(usuario.getSenha())) && usuario.getSenha() != null) {
			String senhaCript = new BCryptPasswordEncoder().encode(usuario.getSenha());
			usuario.setSenha(senhaCript);
		}

		Usuario usuarioSalvo = usuarioRepository.save(usuario);

		return new ResponseEntity<Usuario>(usuarioSalvo, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = "application/json")
	public String delete(@PathVariable("id") Long id) {
		usuarioRepository.deleteById(id);

		return "ok";
	}

}
